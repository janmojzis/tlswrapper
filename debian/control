Source: tlswrapper
Maintainer: Jan Mojžíš <janmojzis@debian.org>
Priority: optional
Standards-Version: 4.7.0
Section: net
Homepage: https://github.com/janmojzis/tlswrapper
Build-Depends: debhelper-compat (= 13),
               lib25519-dev,
               libbearssl-dev,
               librandombytes-dev,
Vcs-Git: https://salsa.debian.org/janmojzis/tlswrapper.git
Vcs-Browser: https://salsa.debian.org/janmojzis/tlswrapper
Rules-Requires-Root: no

Package: tlswrapper
Depends: ${misc:Depends},
         ${shlibs:Depends},
Architecture: any
Description: TLS encryption wrapper
 The tlswrapper is an TLS encryption wrapper between remote client and
 local program prog.
 .
 Internet <--> tcpserver/inetd/systemd.socket/... <--> tlswrapper <--> prog
 .
 Separate process for every connection
 .
 The tlswrapper is executed from systemd.socket/inetd/tcpserver/... which
 runs separate instance of tlswrapper for each TLS connection.
 It ensures that a vulnerability in the code (e.g. bug in the TLS library)
 can't be used to compromise the memory of another connection.
 .
 Separate process for network connection and for secret-key operation
 .
 To protect against secret-information leaks to the network connection
 (such Heartbleed) tlswrapper runs two independent processes for every
 TLS connection. One process holds secret-keys and runs secret-keys operations
 and second talks to the network. Processes communicate with each other through
 unix pipes.
 .
 Privilege separation, filesystem isolation, limits
 .
 The tlswrapper processes run under dedicated non-zero uid to prohibit kill,
 ptrace, etc. Is chrooted into an empty, unwritable directory to prohibit
 filesystem access. Sets ulimits to prohibit new files, sockets, etc.
 Sets ulimits to prohibit forks.
 .
 TLS library
 .
 The tlswrapper is using BearSSL library which implements only secure
 versions of TLS protocol (TLS1.0 - TLS1.2). And implements safe and
 constant-time algorithms.
