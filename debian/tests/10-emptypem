#!/bin/sh
# 20210123
# Jan Mojzis
# Public domain.

set -e

umask 077

dir=`dirname "$0"`

# change directory to $AUTOPKGTEST_TMP
cd "${AUTOPKGTEST_TMP}"

# run tlswrapper port 10000
tcpserver -HRDl0 127.0.0.1 10000 \
sh -c "
  exec 2>tlswrapper.log
  exec tlswrapper -vv -d `pwd`/emptydir -f ./empty.pem sh -c 'cat < data.in'
" 2>&1 &
tcpserverpid=$!
# Give some extra time for tcpserver to start,
# to avoid flaky test failures on slower testbeds
sleep 1

cleanup() {
  ex=$?
  #kill tcpserver
  kill -TERM "${tcpserverpid}" 1>/dev/null 2>/dev/null || :
  kill -KILL "${tcpserverpid}" 1>/dev/null 2>/dev/null || :
  if [ ${ex} -gt 0 ]; then
    (
      echo "tlswrapper.log:"
      cat tlswrapper.log || :
      echo "openssl.log:"
      cat openssl.log || :
    ) >&2
  fi
  rm -rf data.in data.out openssl.log emptydir empty.pem tlswrapper.log
  exit "${ex}"
}
trap "cleanup" EXIT TERM INT

# create emptydir empty.pem
: > empty.pem
mkdir -p emptydir

SCLIENT_CMD="openssl s_client -nocommands -quiet -tls1_2 -verify_return_error"
# create random datafile
dd if=/dev/urandom of=data.in bs=1 count=32 2>/dev/null

if ${SCLIENT_CMD} -servername test -connect 127.0.0.1:10000 1>data.out 2>openssl.log; then
  echo "TLS pem test: failed: tlswrapper accepts empty certificate" >&2
  exit 1
fi
echo "TLS pem test: OK: tlswrapper doesn't accepts empty certificate"
exit 0
